import json
from random import randint

from hata import Client, Embed
from hata.ext.commands import setup_ext_commands

with open("config.json") as f:
    config = json.load(f)
    Vega = Client(token=config["client_1"]["token"], client_id=config["client_1"]["userid"])
    Lily = Client(token=config["client_2"]["token"], client_id=config["client_2"]["userid"])

    setup_ext_commands(Vega, config["client_1"]["prefix"])
    setup_ext_commands(Lily, config["client_2"]["prefix"])
    del(config)
    f.close()

def gethelppage(client, permissions):
    helppage = ""
    for category in client.command_processer.categories:
        if category.name != None:
            helppage = f"{helppage}{category.name}:\n"
            for command in category:
                if set(command.description["required_perms"]).issubset(set(permissions)):
                    helppage = f"{helppage}  - {command.name}\n"
    return Embed(title="Help page", description=helppage, color=randint(255))

@Vega.commands(category="Misc", description={"required_perms":[], "desc":"Just the help page"})
async def help(client, msg):
    await client.message_create(msg.channel, embed=gethelppage(client, msg.channel.permissions_for(msg.author)))

@Lily.commands(category="Misc", description={"required_perms":[], "desc":"Just the help page"})
async def help(client, msg):
    await client.message_create(msg.channel, embed=gethelppage(client, msg.channel.permissions_for(msg.author)))

Vega.start()
Lily.start()
