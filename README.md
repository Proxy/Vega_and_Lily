# Introduction

Hello! I am Technisha and I am making these bots to show off a library called [Hata](https://github.com/HuyaneMatsu/hata) developed by [HuyaneMatsu](https://github.com/HuyaneMatsu)!

# Hosting your own instance

To host your own instance, make 2 bot applications and fill in `example.config.json` and rename it to `config.json`.
The IDs and prefixes do not have to be changed, but it's highly recommended.
